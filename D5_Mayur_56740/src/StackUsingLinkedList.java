import java.util.EmptyStackException;
import java.util.Scanner;

//Every book contains information like name, author, number of pages and price.
//Maximum 5 books can be pushed into the stack. 
//
//Write a menu driven program to perform following operations:
//
//1. push
//
//2. pop
//
//3. top
//
//Also write separate functions for checking stack empty and stack full.
//
//
class Book{
	private String name;
	private String author;
	private int pages;
	private int price;
	public Book() {
		super();
	}
	public Book(String name, String author, int pages, int price) {
		super();
		this.name = name;
		this.author = author;
		this.pages = pages;
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Book [name=" + name + ", author=" + author + ", pages=" + pages + ", price=" + price + "]";
	}
}
	
	class Stack {
		 private Node top;
		 private int size;
		 private int booksCount;
		 private Node  head;
		 private class Node{
			 private Book book;
			 private Node next;
			 private Node prev;
			
			
			public Node(Book book) {
				
				this.book = book;
				this.next = null;
				
				
			}
			
		 }
		 
		  public int booksCount( ){
		        return ( this.booksCount() );
		    }

		    public void addNode(Book book){
		        
		        Node newNode = new Node(book);
		 
		        if( isEmpty() ){
		            head = newNode;
		            booksCount++;
		        }else{
		            Node trav = head;
		         
		            while( trav.next != null ){
		                trav = trav.next;
		            }
		            
		            trav.next = newNode;
		        
		            newNode.prev = trav;
		            booksCount++ ;}
		        }
		    
		    
		    public void delete( ){
		        
		        if( !isEmpty() ){
		            if( head.next == null ){
		               
		                head = null;
		                booksCount--;
		            }else{
		                Node trav = head;
		               
		                while( trav.next.next != null )
		                    trav = trav.next;
		                
		               
		                trav.next = null;
		                booksCount--;
		            }
		        }else
		            System.out.println("list is empty !!!");
		    }
		 
		 public void push(Book book)
		 {
			 
			 if(!isFull())
			 {
				 Node tempNode=new Node(book);
				 tempNode.next=top;
				 top=tempNode;
				 
				 size++;
			 }
			 else
			 {
				 System.out.println("Stack is full");
			 }
			
		 }
		 public Book pop()
		 {
			 if(isEmpty())
			 {
				 throw new EmptyStackException();
			 }
			 Book result=top.book;
			 top=top.next;
			 size--;
			 return result;
		 }
		 
		 public Book peek()
		 {
			 if(isEmpty())
			 {
				 throw new EmptyStackException();
			 }
			 return top.book;
		 }
		 
		 public void displayStack()
		 {
			 Node current =top;
			 while(current!=null)
			 {
				 System.out.println(current.book+" ");
				 current=current.next;
			 }
		 }
		 public int size()
		 {
			 return size;
		 }
		 
		 public boolean isEmpty()
		 {
			 return size==0;
		 }
		 public boolean isFull()
		 {
			 return size>5;
		 }
		}
	
	



public class StackUsingLinkedList {
	  public static int menu(){
          Scanner sc=new Scanner(System.in);
          System.out.println("******* BOOK MENU ********");
          System.out.println("0. Exit ");
          System.out.println("1. Push");
          System.out.println("2. peek");
          System.out.println("3. pop");
          System.out.println("4. add node");
          System.out.println("5. delete node");
          
         
          System.out.println("Enter Your choice");
          int choice=sc.nextInt();
          return choice;
          
      }
	
	
	public static void main(String[] args) {
		Stack s=new Stack();
		Book book;
		
		 Scanner sc=new Scanner(System.in);
		 
		 while(true)
		 {
			 int choice=menu();
             switch (choice) {
                 case 0:
                    System.exit(0);
                 break;
                 
                 case 1:
                	if(!s.isFull())
                	{// name, author, number of pages and price.
                		 System.out.println("Enter Book Details ");
                    	 
                    	 System.out.println("Enter name of a book");
                          String name=sc.next();
                          System.out.println("Who is the author?? ");
                          String author=sc.next();
                          System.out.println("How many pages book does have??");
                          int pages=sc.nextInt();
                          System.out.println("What is the price?? ");
                          int price=sc.nextInt();
                          book=new Book(name, author, pages, price);
                         // book=new Box(b_color, length, depth, breadth);
                          s.push(book);
                          System.out.println(book+" added into the stack");
                          System.out.println(":-)");
                	}
                	else
                	{
                		System.out.println("stack is full,cant add another book");
                		 System.out.println(":-(");
                	}
                      
                	 break;
                	 
                 case 2:
                	  if( !s.isEmpty() ){
                          book = s.peek();
                          System.out.println("top most book is : "+book);
                      }else
                          System.out.println("stack underflow,you havent added any book till now :-( !!!");
                      break;
                	
                	   
                    case 3:
                    	 if( !s.isEmpty() ){
                             book = s.peek();
                             s.pop();
                             System.out.println(book+" is from the the stack....");
                         }else
                             System.out.println("stack underflow !!!");

                      
                    	 
                    	 break;
                    	 
                    case 4:
                          System.out.println("Enter Book Details ");
                    	 
                    	 System.out.println("Enter name of a book");
                          String name=sc.next();
                          System.out.println("Who is the author?? ");
                          String author=sc.next();
                          System.out.println("How many pages book does have??");
                          int pages=sc.nextInt();
                          System.out.println("What is the price?? ");
                          int price=sc.nextInt();
                          book=new Book(name, author, pages, price);
                         // book=new Box(b_color, length, depth, breadth);
                          s.push(book);
                          System.out.println("node added");
                          System.out.println(":-)");            

                            break;
                            
                    case 5:
                    	
                    	 s.delete();
                         System.out.println("Deleted successfully");
                    break;

                	 
                      
                 default:
                	 System.out.println("Invalid Option,choose the correct option");
                     break;
                	 
                	 
                	 
           
             }
		 }
		 
		 
	}


}
